\NeedsTeXFormat{LaTeX2e}

\LoadClass{article}
\ProvidesClass{exercisesheet}

\setlength{\parindent}{0pt}

\RequirePackage{geometry}
\geometry{
	a4paper,
	left = 2cm,
	right = 2cm,
	top = 1.5cm,
	bottom = 2cm
}


\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{amsmath, amsfonts, amssymb, amsthm}
\RequirePackage{multicol}
\RequirePackage{listings}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\usetikzlibrary{shapes, arrows, positioning, patterns, decorations.pathreplacing}

\RequirePackage{totcount}
\RequirePackage{changepage}
\RequirePackage{etoolbox}
\RequirePackage{environ}

\RequirePackage{booktabs}
\RequirePackage{mathtools} 
\RequirePackage{xstring} 
\RequirePackage{enumitem}
\RequirePackage{hhline}
\RequirePackage{wasysym}
\RequirePackage{latexsym}

\newcommand{\longequal}[1]{\mathop{\overset{\mathrm{#1}}{\resizebox{\widthof{\ensuremath{\mathop{\overset{\mathrm{#1}}{=}}}}}{\heightof{=}}{=}}}}
\newcommand{\n}{~\\}
\newcommand{\seperator}{~|~}
\newcommand{\sgn}{\text{sgn}}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newcommand{\abs}[1]{\left|#1\right|}

% Aufgaben-Commands
% \aufgabe{Punktzahl}
\newtotcounter{exercisenumber}
\newtotcounter{subexercisenumber}
\newtotcounter{totalpoints}

\newcommand{\Exercise}{Exercise}

\newenvironment{exercise}[1]
{
	\def\points{#1}
	\stepcounter{exercisenumber}
	\setcounter{subexercisenumber}{0}
	\section*{\Exercise~\arabic{exercisenumber}}
}
{	
	\phantom{} \hfill A\arabic{exercisenumber}: \framebox[1.75cm]{\hspace{1cm}/\hfill\points\hspace{5pt}} \\
	\addtocounter{totalpoints}{100*\real{\points}}
	\addPT{\arabic{exercisenumber}}{\points}
}

\newenvironment{subexercise}[1][0]
{
	\stepcounter{subexercisenumber}
	
	\ifnum\value{subexercisenumber}=1
	\else
		\vspace{1em}
	\fi
	
	\textbf{(\alph{subexercisenumber})}
	\begin{adjustwidth}{2em}{0em}
	\vspace{-\baselineskip}
}
{
	\end{adjustwidth}
}

\newcommand{\addPT}[2]
{
	    \edef\tempA{A#1 &}
	    \edef\tempB{\qquad/#2 &}
	
	\expandafter\gappto\expandafter\PTableA\expandafter{\tempA}
	\expandafter\gappto\expandafter\PTableB\expandafter{\tempB}
	\expandafter\gappto\expandafter\PTHead\expandafter{c | }
}

\newcommand{\pointtable}{
    \begin{table}[h!]
        \centering
        \ifcsname PTableASaved\endcsname
        \begin{tabular}{\PTHeadSaved c}
            \PTableASaved $\Sigma$ \\ \hline
        	\PTableBSaved \qquad / \StrGobbleRight{\totalpointsSaved}{2}.\StrRight{\totalpointsSaved}{2}
        	%\PTableBSaved \qquad / \totalpointsSaved
    	\end{tabular}
        \else
        Compile again\ClassWarning{Compile again}
        \fi
    \end{table}
}
\AtEndDocument{
    \makeatletter
    \immediate\write\@mainaux{\string\gdef\string\PTableASaved{\PTableA}}%
    \immediate\write\@mainaux{\string\gdef\string\PTableBSaved{\PTableB}}%
    \immediate\write\@mainaux{\string\gdef\string\PTHeadSaved{\PTHead}}%
    \setcounter{totalpoints}{\totvalue{totalpoints}}
    \immediate\write\@mainaux{\string\gdef\string\totalpointsSaved{\arabic{totalpoints}}}%
    \makeatother
}